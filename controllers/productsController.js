var db = require('../db/models');

exports.post = (req, res) => {
    const arrItemsRequest = req.body;   // Almancena los items enviados en la petición HTTP.
    let   arrProductsPrice = [];        // Almacena la cantidad de items enviados así como el precio de cada uno de ellos.
    let   arrKeys = [];                 // Almacena el nombre de los items enviados para ser buscados en la base de datos.
    let   totalPrice = 0;               // Precio total de la suma de los precios de los productos.

    if ( Array.isArray(arrItemsRequest) && !!arrItemsRequest.length )
    { // if - La petición es un array y no es vacio - TOP

        contarItemsEnArrayRequest(arrItemsRequest, arrProductsPrice, arrKeys);


        // Búsqueda de los items (arrKeys) en la base de datos
        db.products.findAll({
            where: {code: arrKeys},
            attributes: ["code", "price", "promo"],
            raw: true
        }).then( arrProducts => {

            arrProducts.forEach( productDB => {
                // foreach - Recorremos el array arrojdo por la consulta para saber los precios de los productos - TOP

                let p = buscarEnArray(arrProductsPrice, productDB.code);

                if ( p != undefined)
                { // El producto existe en la base de datos y se le asigna el precio - TOP
                    p.price = productDB.price;
                    p.promo = productDB.promo;
                } // El producto existe en la base de datos y se le asigna el precio - BOTTOM
            }); // foreach - Recorremos el array arrojdo por la consulta para saber los precios de los productos - BOTTOM

            arrProductsPrice.forEach( prod => {
                 // foreach - Recorremos el array de productos para hacer el cálculo del total a pagar - TOP
                totalPrice += obtenerPrecioProducto( prod );
            });  // foreach - Recorremos el array de productos para hacer el cálculo del total a pagar - BOTTOM

            return res.json(totalPrice);
        }).catch( error => {
            console.error(`Ha ocurrido un error al tratar de obtener los productos de la base de datos: ${error}`);
            return res.json("");
        });
    } // if - La petición es un array y no es vacio - BOTTOM
    else
    { // else - La petición es un array vacio o no es un tipo de dato array - TOP
        console.log("La petición no tiene un formato aceptado o no tiene datos en el cuerpo del mensaje");
        return res.json("");
    } // else - La petición es un array vacio o no es un tipo de dato array - BOTTOM


};


function contarItemsEnArrayRequest(arrItemsRequest, arrProductsPrice, arrKeys) { // contarItemsEnArrayRequest - Contabiliza los items recibidos y los agrega a los array arrProductsPrice y arrKeys - TOP
    arrItemsRequest.forEach(item => {
         // foreach - Recorre el arrray recibido y verifica para contabilizar la cantidad de cada item enviado - TOP

        let newItem = buscarEnArray(arrProductsPrice, item);

        if (newItem == undefined)
        { // if - El item no existe, es agregado al array de productos y al array de llaves - TOP
            newItem = {name: item, quantity: 0, price: 0, promo: 0};
            arrProductsPrice.push(newItem);
            arrKeys.push(item);
        } // if - El item no existe, es agregado al array de productos y al array de llaves - BOTTOM

        newItem.quantity++;

    });  // foreach - Recorre el arrray recibido y verifica para contabilizar la cantidad de cada item enviado - BOTTOM
} // contarItemsEnArrayRequest - Contabiliza los items recibidos y los agrega a los array arrProductsPrice y arrKeys - BOTTOM

function obtenerPrecioProducto( prod )
{ // obtenerPrecioProducto - Obtiene el precio total de una cantidad de productos del mismo tipo dependiendo de la promoción que tenga asociada - TOP
    let precio = 0;

    switch ( prod.promo )
    {
        case 1: // Promoción 2 x 1
            if ( prod.quantity == 1 )
            { // if - Hay una sola unidad de PANTS en el array - TOP
                precio = prod.quantity * prod.price;
            } // if - Hay una sola unidad de PANTS en el array - BOTTOM
            else if ( (prod.quantity % 2) == 1 )
            { // Hay una cantidad impar de pants en el array - TOP
                precio = ( ((prod.quantity - 1) / 2) + 1 ) * prod.price;
            } // Hay una cantidad impar de pants en el array - BOTTOM
            else if ( (prod.quantity / 2) >= 1 )
            { // Hay una cantidad par de pants en el array - TOP
                precio = ( prod.quantity / 2)  * prod.price;
            } // Hay una cantidad par de pants en el array - BOTTOM
            break;
        case 2: // Promooción de descuento
            if ( prod.quantity >= 3 )
            { // Hay más de 3 playeras en el array - TOP
                precio = prod.quantity * ( prod.price - 1 );
            } // Hay más de 3 playeras en el array - BOTTOM
            else
            { // Hay menos de 3 playeras en el array - TOP
                precio = prod.quantity * prod.price;
            } // Hay menos de 3 playeras en el array - BOTTOM
            break;
        default : // Productos si promoción
            precio = prod.quantity * prod.price;
    }

    return precio;
} // obtenerPrecioProducto - Obtiene el precio total de una cantidad de productos del mismo tipo dependiendo de la promoción que tenga asociada - BOTTOM

function buscarEnArray(array, elementoBuscado)
{ // buscarEnArray - Busca un elemento en el array - TOP
    return array.find( e => {
       return e.name == elementoBuscado;
    });
} // buscarEnArray - Busca un elemento en el array - BOTTOM