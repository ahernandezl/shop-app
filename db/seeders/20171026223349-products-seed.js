'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('products', [
          {
            code:  'PANTS',
            name:  'Pants',
            price: 5,
            promo: 1,
            createdAt: new Date(),
            updatedAt: new Date(),
          },
          {
              code:  'TSHIRT',
              name:  'T-Shirt',
              price: 20,
              promo: 2,
              createdAt: new Date(),
              updatedAt: new Date(),
          },
          {
              code:  'HAT',
              name:  'Hat',
              price: 7.5,
              promo: 0,
              createdAt: new Date(),
              updatedAt: new Date(),
          }], {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('products', [
          { code :'PANTS' },
          { code :'TSHIRT' },
          { code :'HAT' },
      ], {});
  }
};
